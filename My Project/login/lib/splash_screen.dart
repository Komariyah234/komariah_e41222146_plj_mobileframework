import 'dart:async';

import 'package:flutter/material.dart';
import 'package:login/login.dart';

class SplashScreen extends StatefulWidget {
  // const SplashScreen ({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => InitState();

  }

  class InitState extends State<SplashScreen> {


    @override
    void initState() {
      super.initState();
      startTimer();
    }

    startTimer() async {
      var duration = const Duration(seconds: 5);
      return Timer(duration, loginRoute);
    }

    loginRoute() {
      Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => Login()
          ));
    }

    @override
    Widget build(BuildContext context) {
      return initWidget();
    }

    Widget initWidget() {
      return Scaffold (
        body: Stack(
          children: [
            Container (
              decoration: const BoxDecoration(
                color:  Color(0xff2074B1),
                gradient: LinearGradient(
                  colors: [( Color (0xff2074B1)), ( Color(0xff2074B1))],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter
                  )
              ),
            ),

            Center(
              // child: Container(
                child: Image.asset('assets/images/logo.png'),
              // ),
            )
          ],
        ),
      );
    
    }
  }