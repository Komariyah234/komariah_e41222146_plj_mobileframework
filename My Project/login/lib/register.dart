// import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:login/login.dart';
import 'package:login/splash_screen.dart';
import 'package:login/register.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => InitState();
}

class InitState extends State<Register> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPass = TextEditingController();
  void kirimData() {
    AlertDialog alertDialog = AlertDialog(
      content: SizedBox(
        height: 200.0,
        child: Column(children: <Widget>[
          const Text(
            "WELCOME TO",
            textAlign: TextAlign.center,
          ),
          Text(
            "Name: ${controllerEmail.text}",
            textAlign: TextAlign.left,
          ),
          Text(
            "No. Handphone: ${controllerEmail.text}",
            textAlign: TextAlign.left,
          ),
          Text(
            "Email: ${controllerEmail.text}",
            textAlign: TextAlign.left,
          ),
          Text(
            "Password: ${controllerPass.text}",
            textAlign: TextAlign.left,
          ),
          Padding(
            padding: const EdgeInsets.all(8),
            child: ElevatedButton(
                onPressed: () {
                  Route route =
                      MaterialPageRoute(builder: (context) => Login());
                  Navigator.push(context, route);
                },
                // => Navigator.pop(context),
                // style: ElevatedButton.styleFrom(backgroundColor: Colors.teal),
                child: const Text("OK")),
          ),
        ]),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        //key: _formKey,
        children: <Widget>[
          // Container(
          //   width: 400,
          //   height: 450,
          //   decoration: BoxDecoration(
          //     image: DecorationImage(
          //       image: AssetImage('assets/images/Rectangle_login.png'),
          //       fit: BoxFit.fitHeight,
          //       ),
          //   ),
          // ),
          Positioned(
            top: 0,
            width: 400,
            child: Image.asset("assets/images/Rectangle_login.png"),
          ),

          const Positioned(
              top: 90,
              left: 15.0,
              right: 100.0,
              child: Text("Sign Up",
                  style: TextStyle(
                    fontSize: 47.0,
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ))),
          const Positioned(
              top: 149.0,
              left: 18.0,
              right: 200.0,
              child: Text("Hokimusic Guitar",
                  style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                      fontWeight: FontWeight.normal))),

          Positioned(
            top: 400,
            child: Container(
              height: 430,
              width: MediaQuery.of(context).size.width - 40,
              margin: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        blurRadius: 15,
                        spreadRadius: 5),
                  ]),
              child: Column(
                children: [
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.start,
                  //   children: [
                  //     Padding(padding: EdgeInsets.fromLTRB(8, 22, 8, 22)),
                  //     Text("Email",
                  //         style: TextStyle(
                  //             fontSize: 15.0,
                  //             fontFamily: 'alice',
                  //             color: Color.fromRGBO(0, 0, 0, 0.97),
                  //             fontWeight: FontWeight.normal)),
                  //     Padding(padding: EdgeInsets.fromLTRB(8, 22, 8, 22)),
                  //   ],
                  // ),
                  Container(
                    padding: EdgeInsets.all(20),
                    margin: EdgeInsets.only(top: 15, left: 6, right: 6),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        TextFormField(
                          //controller: controllerEmail,
                          keyboardType: TextInputType.emailAddress,
                          decoration: const InputDecoration(
                              prefixIcon: Icon(Icons.people),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              hintText: 'komariyah',
                              labelText: 'Name'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Nama masih kosong';
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          //controller: controllerEmail,
                          keyboardType: TextInputType.emailAddress,
                          decoration: const InputDecoration(
                              prefixIcon: Icon(Icons.call),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              hintText: '082xxxxxx',
                              labelText: 'No. Hanphone'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'No. Hp masih kosong';
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          controller: controllerEmail,
                          keyboardType: TextInputType.emailAddress,
                          decoration: const InputDecoration(
                              prefixIcon: Icon(Icons.email_outlined),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              hintText: 'komariyah@gmail.com',
                              labelText: 'Email'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Email masih kosong';
                            }
                            return null;
                          },
                        ),
                       // Padding(padding: EdgeInsets.only(top: 8)),
                        TextFormField(
                          controller: controllerPass,
                          obscureText: true,
                          keyboardType: TextInputType.emailAddress,
                          decoration: const InputDecoration(
                              prefixIcon: Icon(Icons.lock_outline),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              // hintText: '12345678',
                              labelText: 'Password'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Password masih kosong';
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          controller: controllerPass,
                          obscureText: true,
                          keyboardType: TextInputType.emailAddress,
                          decoration: const InputDecoration(
                              prefixIcon: Icon(Icons.lock_outline),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              // hintText: '12345678',
                              labelText: 'Confirm Password'),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Password masih kosong';
                            }
                            return null;
                          },
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        
                        Padding(padding: EdgeInsets.fromLTRB(8, 20, 8, 22)),
                        SizedBox(
                          height: 0,
                        ),
                        ElevatedButton(
                            onPressed: () {
                              // final bool? isValid =
                              //     _formKey.currentState?.validate();
                              // if (isValid == true) {
                              //   kirimData();
                              // }
                              kirimData();
                            },
                            child: const Text('Continue')),
                        GestureDetector(
                          onTap: () {
                            // Route route = MaterialPageRoute(
                            //     builder: (context) => Login());
                            // Navigator.push(context, route);
                            kirimData();
                          },
                          
                          child: Text("Login",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 12.0,
                                  height: 2,
                                  color: Color.fromARGB(255, 98, 188, 253),
                                  fontWeight: FontWeight.normal)),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
