import 'package:flutter/material.dart';
import 'package:gradient_pageview_dropdown/pilihan.dart';

// class HalamanDua extends StatelessWidget { //? utk pageview
class HalamanDua extends StatefulWidget {
  const HalamanDua({
    Key? key,
    required this.gambar,
    required this.color,
  }) : super(key: key);

  final String gambar;
  final Color color;

//? DROPDOWN
  @override
  State<HalamanDua> createState() => _HalamanDuaState();
}

class _HalamanDuaState extends State<HalamanDua> {
  Color warna = Colors.grey;

  List<Pilihan> listPilihan = [
    Pilihan(teks: 'Red', warna: Colors.red),
    Pilihan(teks: 'Green', warna: Colors.green),
    Pilihan(teks: 'Blue', warna: Colors.blue),
  ];

  void _onSelectColor(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BT21'),
        backgroundColor: Colors.purpleAccent,
        //! dropdwon
        actions: [
          //? DROPDOWN
          PopupMenuButton<Pilihan>(
            onSelected: _onSelectColor,
            itemBuilder: (context) => listPilihan
                .map(
                  (pilihan) => PopupMenuItem<Pilihan>(
                    value: pilihan,
                    child: Text(pilihan.teks),
                  ),
                )
                .toList(),
          )
        ], //! end
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: RadialGradient(
                center: Alignment.center,
                colors: [Colors.purple, Colors.white, Colors.deepPurple],
              ),
            ),
          ),
          Center(
            child: Hero(
              // tag: gambar, //? pageview
               tag: widget.gambar, //? dropdown
              child: ClipOval(
                child: SizedBox(
                  width: 200,
                  height: 200,
                  child: Material(
                    child: InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      child: Flexible(
                        flex: 1,
                        child: Container(
                          // color: color, //? pageview
                          color: widget.color, //? dropdown
                          child: Image.asset(
                            // 'assets/images/$gambar', //? pageview
                             'assets/images/${widget.gambar}', //? dropdown
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

