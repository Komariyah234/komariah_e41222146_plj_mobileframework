import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx/controller/demoController.dart';
import 'package:getx/controller/purchase.dart';

class HomePage extends StatelessWidget{
  final purchase = Get.put(Purchase());
  DemoController cart = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        ),
        bottomSheet: SafeArea(
          child: Card(
            elevation: 12.0,
            margin: EdgeInsets.zero,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.blue,
                ),
                height: 65,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Stack(
                        children: [
                          Icon(Icons.shopping_cart_rounded,
                          size: 40, color: Colors.white),
                          Positioned(
                            right: 5,
                            child: Container(
                              height: 20,
                              width: 20,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.red,
                              ),
                              child: Center(
                                child: GetX<DemoController>(builder: (controller) {
                                  return Text(
                                    '${controller.cartCount}',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 11.0),
                                  );
                                }),
                              ),
                            ),
                            ),
                        ],
                      ),
                      GetX<DemoController>(builder: (controller) {
                        return Text(
                          'Total Amount - ${controller.totalAmount}',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w300,
                            fontSize: 16.0),
                        );
                      }),
                      IconButton(
                        onPressed: () => Get.toNamed('/cart',
                        arguments:
                              "Home Page To Demo Page -> Passing arguments"),
                        icon: Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Colors.white,
                        ))
                    ],
                  ),
                  ),
                ),
          ),
          ),
          body: Container,
    )
  }
}