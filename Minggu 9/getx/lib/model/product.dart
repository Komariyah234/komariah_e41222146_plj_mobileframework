class Product {
  final int id;
  final String productName;
  final String productImage;
  final String productDescription;
  final String price;

  Product(this.id, this.productName, this.productImage, this.productDescription, this.price);
}