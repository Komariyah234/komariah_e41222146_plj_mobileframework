import 'dart:io';

 void main(List<String> args) {
//   // * 1. Ternary Operator
//   //un comment kode dibawah untuk menjalankan
//    print('Ingin menginstall aplikasi? (y/t)');
//    String? answer = stdin.readLineSync();
//    answer = answer?.toLowerCase();
//    answer == 'y';
//         print('Anda akan menginstall aplikasi dart')
//        ; print('aborted');

  // * 2. If-else if dan else
  // un comment kode dibawah untuk menjalankan
  // print('Selamat datang di Dunia Werewolf');
  // print('Masukkan nama anda:');
  // String? nama = stdin.readLineSync();
  // // jika tidak memilih nama
  // if (nama == null) {
  //   print('Nama harus diisi!');
  //   return;
  // }

  // print(
  //     'Halo $nama. Pilih peranmu untuk memulai game (penyihir | guard | werewolf):');
  // String? peran = stdin.readLineSync();
  // // jika tidak memilih peran
  // if (peran == null) {
  //   print('Peran harus diisi!');
  //   return;
  // }

  // print('Selamat datang di Dunia Werewolf, $nama');
  // if (peran.toLowerCase() == 'penyihir') {
  //   // jika penyihir
  //   print(
  //       'Halo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!');
  // } else if (peran.toLowerCase() == 'guard') {
  //   print(
  //       'Halo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf.');
  // } else if (peran.toLowerCase() == 'werewolf') {
  //   print('Halo Werewolf $nama, Kamu akan memakan mangsa setiap malam!');
  // } else {
  //   print('Halo $nama, peranmu tidak dikenali');
  // }

  // * 3. Switch Case
  // un comment kode dibawah untuk menjalankan
  // print('Day by Day Quotes');
  // print('Pilih hari (senin | selasa | rabu | kamis | jumat | sabtu | minggu)');
  // String? hari = stdin.readLineSync();
  // switch (hari?.toLowerCase()) {
  //   case 'senin':
  //     print(
  //         'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja');
  //     break;
  //   case 'selasa':
  //     print(
  //         'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
  //     break;
  //   case 'rabu':
  //     print(
  //         'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
  //     break;
  //   case 'kamis':
  //     print(
  //         'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
  //     break;
  //   case 'jumat':
  //     print('Hidup tak selamanya tentang pacar.');
  //     break;
  //   case 'sabtu':
  //     print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
  //     break;
  //   case 'minggu':
  //     print(
  //         'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
  //     break;
  //   default:
  //     print('Quotes tidak ditemukan');
  // }

  // * 4. Switch Case
  // un comment kode dibawah untuk menjalankan
  // int hari = 14;
  // int bulan = 12;
  // int tahun = 1999;

  // String namaBulan = '';

  // switch (bulan) {
  //   case 1:
  //     namaBulan = 'Januari';
  //     break;
  //   case 2:
  //     namaBulan = 'Februari';
  //     break;
  //   case 3:
  //     namaBulan = 'Maret';
  //     break;
  //   case 4:
  //     namaBulan = 'April';
  //     break;
  //   case 5:
  //     namaBulan = 'Mei';
  //     break;
  //   case 6:
  //     namaBulan = 'Juni';
  //     break;
  //   case 7:
  //     namaBulan = 'Juli';
  //     break;
  //   case 8:
  //     namaBulan = 'Agustus';
  //     break;
  //   case 9:
  //     namaBulan = 'September';
  //     break;
  //   case 10:
  //     namaBulan = 'Oktober';
  //     break;
  //   case 11:
  //     namaBulan = 'November';
  //     break;
  //   case 12:
  //     namaBulan = 'Desember';
  //     break;
  //   default:
  //     print('bulan tidak diketahui');
  //     return;
  // }

  // print('$hari $namaBulan $tahun');

  // * 5 Looping While
  // un comment kode dibawah untuk menjalankan

  // int index = 2;
  // bool isAscending = true;
  // while (index >= 2) {
  //   if (index == 2 && isAscending) {
  //     print('LOOPING PERTAMA');
  //   }

  //   if (isAscending) {
  //     print('$index - I Love coding');
  //   } else {
  //     print('$index - I will become a mobile developer');
  //   }

  //   if (index < 20 && isAscending) {
  //     index += 2;
  //   } else {
  //     if (isAscending) {
  //       print('LOOPING KEDUA');
  //       print('$index - I will become a mobile developer');
  //     }
  //     index -= 2;
  //     isAscending = false;
  //   }
  // }

  // * 6. Looping For
  // un comment kode dibawah untuk menjalankan perogram
  // for (var i = 1; i <= 20; i++) {
  //   // jika habis dibagi 3 dan ganjil
  //   if ((i % 3 == 0) && (i % 2 != 0)) {
  //     print('$i - I Love Coding');
  //     continue;
  //   }

  //   // jika genap
  //   if (i % 2 == 0) {
  //     print('$i - Berkualitas');
  //     continue;
  //   }

    // jika ganjil
  //   print('$i - Santai');
  // }

  // * 9. Membuat Persegi Panjang
  // un comment kode dibawah untuk menjalankan
  for (var i = 0; i < 4; i++) {
    print('########');
  }

  // * 10. Membuat Tangga
  // un comment kode dibawah untuk menjalankan
  // for (var i = 1; i <= 8; i++) {
  //   String hash = '';
  //   int count = 1;

  //   while (count <= i) {
  //     hash += '#';
  //     count++;
  //   }
  //   print(hash);
  // }
}
