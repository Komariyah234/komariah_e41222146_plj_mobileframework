//? menjalankan premis bernilai true
// void main() {
//  if ( true) {
//  print("jalankan code");
//  }
// }

//? menjalankan premis bernilai false
// void main() {
//  if ( false ) {
//  print("Program tidak jalan code");
//  }
// }

//? Premis dengan perbandingan suatu nilai
// void main() {
// var kondisiMentalku = "sedih";
//  if ( kondisiMentalku == "happy" ) {
//  print("hari ini aku bahagia!");
//  }
// }

//?  Branching sederhana
// void main() {
//  var minimarketStatus = "close";
//  if (minimarketStatus == "open") {
//  print("saya akan membeli telur dan buah");
//  } else {
//  print("minimarketnya tutup");
//  }
// }

//? Branching dengan kondisi
// void main() {
//  var minimarketStatus = "open";
//  var minuteRemainingToOpen = 5;
//  if (minimarketStatus == "open") {
//  print("saya akan membeli telur dan buah");
//  } else if (minuteRemainingToOpen <= 5) {
//  print("minimarket buka $minuteRemainingToOpen menit lagi sebentar lagi, saya tungguin");
//  } else {
//  print("minimarket tutup, saya pulang lagi");
//  }
// }

//? Kondisional bersarang atau kondisional dengan if else

// void main() {
//   var minimarketStatus = "open";
//   var telur = "tersedia";
//   var buah = "tersedia";
//   if (minimarketStatus == "open") {
//     print("saya akan membeli telur dan buah");

//     if (telur == "soldout" || buah == "soldout") {
//       print("belanjaan saya tidak lengkap");
//     } else if (telur == "soldout") {
//       print("telur habis");
//     } else if (buah == "soldout") {
//       print("buah habis");
//     }
//   } else {
//     print("minimarket tutup, saya pulang lagi");
//   }
// }


//?kondisional dengan switch case
// void main() {
// var buttonPushed = 2;
// switch(buttonPushed) {
//  case 1: { print('matikan TV!'); break; }
//  case 2: { print('turunkan volume TV!'); break; }
//  case 3: { print('tingkatkan volume TV!'); break; }
//  case 4: { print('matikan suara TV!'); break; }
//  default: { print('Tidak terjadi apa-apa'); }}
// }

