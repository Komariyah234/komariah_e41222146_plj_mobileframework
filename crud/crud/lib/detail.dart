import 'dart:developer';
import 'dart:js';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './editdata.dart';
import './main.dart';

class Detail extends StatefulWidget {
  List list;
  int index;
  Detail({
    Key? key,
    required this.list,
    required this.index,
  }) : super(key: key);
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  void deleteData() {
    var url = 'http://127.0.0.1:8000/api/produk/';
    final uri = Uri.parse(url + widget.list[widget.index]['id'].toString());
    log(uri.toString());
    http.delete(uri);
    // body: {
    //   'id': widget.list[widget.index]['id'].toString()
    // });
  }

  void confirm(BuildContext context) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(
          "Are You sure want to delete '${widget.list[widget.index]['nama_produk']}'"),
      actions: <Widget>[
        ElevatedButton(
          child: Text(
            "OK DELETE",
            style: TextStyle(color: Colors.black),
          ),
          style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
          onPressed: () {
            deleteData();
            Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => Home(),
            ));
          },
        ),
        ElevatedButton(
          child: Text("CANCEL", style: TextStyle(color: Colors.black)),
          style: ElevatedButton.styleFrom(backgroundColor: Colors.green),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );

    showDialog(builder: (context) => alertDialog, context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          AppBar(title: Text("${widget.list[widget.index]['nama_produk']}")),
      body: Container(
        height: 270.0,
        padding: const EdgeInsets.all(20.0),
        child: Card(
          child: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                ),
                Text(
                  widget.list[widget.index]['nama_produk'],
                  style: TextStyle(fontSize: 20.0),
                ),
                Text(
                  "Harga_Produk: ${widget.list[widget.index]['harga_produk']}",
                  style: TextStyle(fontSize: 18.0),
                ),
                Text(
                  "Deskripsi: ${widget.list[widget.index]['deskripsi_produk']}",
                  style: TextStyle(fontSize: 18.0),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ElevatedButton(
                      child: Text("EDIT"),
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      onPressed: () =>
                          Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => EditData(
                          list: widget.list,
                          index: widget.index,
                        ),
                      )),
                    ),
                    ElevatedButton(
                      child: Text("DELETE"),
                      style:
                          ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      onPressed: () => confirm(context),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
