import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'main.dart';

class AddData extends StatefulWidget {
  const AddData({Key? key}) : super(key: key);

  @override
  _AddDataState createState() => _AddDataState();
}

class _AddDataState extends State<AddData> {
  TextEditingController controllerNamaProduk = TextEditingController();
  TextEditingController controllerHargaProduk = TextEditingController();
  TextEditingController controllerDeskripsi = TextEditingController();

  void addData() {
    var url = "http://127.0.0.1:8000/api/produk";

    http.post(Uri.parse(url), body: {
      "nama_produk": controllerNamaProduk.text,
      "harga_produk": controllerHargaProduk.text,
      "deskripsi_produk": controllerDeskripsi.text
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ADD DATA"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                TextField(
                  controller: controllerNamaProduk,
                  decoration: InputDecoration(
                      hintText: "Nama Produk", labelText: "Nama Produk"),
                ),
                TextField(
                  controller: controllerHargaProduk,
                  decoration: InputDecoration(
                      hintText: "Harga Produk", labelText: "Harga Produk"),
                ),
                TextField(
                  controller: controllerDeskripsi,
                  decoration: InputDecoration(
                      hintText: "Deskripsi", labelText: "Deskripsi_produk"),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                ),
                ElevatedButton(
                  child: Text("ADD DATA"),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blueAccent),
                  onPressed: () {
                    addData();
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => Home()));
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
