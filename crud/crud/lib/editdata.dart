import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './main.dart';

class EditData extends StatefulWidget {
  final List list;
  final int index;

  EditData({required this.list, required this.index, id});

  @override
  _EditDataState createState() =>  _EditDataState();
}

class _EditDataState extends State<EditData> {
  late TextEditingController controllerNamaProduk;
  late TextEditingController controllerHargaProduk;
  late TextEditingController controllerDeskripsi;

  void editData() async {
    http.put(
      Uri.parse('http://127.0.0.1:8000/api/produk/' +
      widget.list[widget.index]['id'].toString()),
      headers: {
        'Accept': 'application/json',
      },
      body: {
        "nama_produk": controllerNamaProduk.text,
        "harga_produk": controllerHargaProduk.text,
        "deskripsi_produk": controllerDeskripsi.text,
      }).then((response) {
        print('Response status : ${response.statusCode}');
        print('Response body : ${response.body}');
      });
  }

  @override
  void initState() {
    controllerNamaProduk =
     TextEditingController(text: widget.list[widget.index]['nama_produk']);
    controllerHargaProduk =  TextEditingController(
      text: widget.list[widget.index]['harga_produk'].toString());
    controllerDeskripsi =  TextEditingController(
      text: widget.list[widget.index]['deskripsi_produk'].toString());
      super.initState();
  }

  @override 
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar:  AppBar(
        title:  const Text("EDIT DATA"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
             Column(
              children: <Widget>[
                 TextField(
                  controller: controllerNamaProduk,
                  decoration: 
                  const InputDecoration(hintText: "Nama Produk", labelText: "Nama_Produk"),
                ),
                 TextField(
                  controller: controllerHargaProduk,
                  decoration: 
                   const InputDecoration(
                    hintText: "Harga Produk", labelText: "Harga_Produk"),
                ),
                 TextField(
                  controller: controllerDeskripsi,
                  decoration: 
                  const InputDecoration(
                    hintText: "Deskripsi", labelText: "Deskripsi_produk"),
                ),
                 const Padding(
                  padding: EdgeInsets.all(10.0),
                  ),
                   ElevatedButton(
                    child:  const Text("EDIT DATA"),
                    style: ElevatedButton.styleFrom(backgroundColor: Colors.blue),                    
                    onPressed: () {
                      editData();
                      Navigator.of(context).push( MaterialPageRoute(
                        builder: (BuildContext context) =>  Home()));                        
                    },
                    )
              ],
            ),
          ],
        ),
        ),
    );
  }

}