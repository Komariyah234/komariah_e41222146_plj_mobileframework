import 'package:flutter/material.dart';
import 'package:sidebar/detail.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // List<Container> daftarSuperhero = List.filled(10, index);
  // var karakter = [
  //   {"nama": "Aquaman", "gambar": "aquaman.jpg"},
  //   {"nama": "Batman", "gambar": "batman.jpg"},
  //   {"nama": "Captain Amerika", "gambar": "captain.jpg"},
  //   {"nama": "Catwoman", "gambar": "catwoman.jpg"},
  //   {"nama": "Flash", "gambar": "flash.jpg"},
  //   {"nama": "Hulk", "gambar": "hulk.jpg"},
  //   {"nama": "Ironman", "gambar": "ironman.jpg"},
  //   {"nama": "Spiderman", "gambar": "spiderman.jpg"},
  //   {"nama": "Venom", "gambar": "venom.jpg"},
  //   {"nama": "Superman", "gambar": "superman.jpg"},
  // ];
  //
  // _generateList() async {
  //   for (var i = 0; i < karakter.length; i++) {
  //     final karakternya = karakter[i] as int;
  //     daftarSuperhero.add(
  //       Container(child: Text(karakternya[1])),
  //     );
  //   }
  // }

  int _counter = 0;

  String gambar1 =
      "https://scholar.googleusercontent.com/citations?view_op=view_photo&user=a_KipnEAAAAJ&citpid=1";
  String gambar2 =
      "https://scholar.googleusercontent.com/citations?view_op=view_photo&user=OFD_sjQAAAAJ&citpid=2";
  late String backup;

  String nama1 = "I Gede Wiryawan";
  String nama2 = "Syamsiar Kautsar";
  late String backupNama;

  @override
  void initState() {
    // _generateList();
    super.initState();
  }

  void gantiUser() {
    setState(() {
      backup = gambar1;
      gambar1 = gambar2;
      gambar2 = backup;

      backupNama = nama1;
      nama1 = nama2;
      nama2 = backupNama;
    });
  }

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Sidebar"),
        backgroundColor: Colors.red[600],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(nama1),
              accountEmail: const Text("igdw@windowslive.com"),
              currentAccountPicture: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          Detail(nama: nama1, gambar: gambar1)));
                },
                child: CircleAvatar(backgroundImage: NetworkImage(gambar1)),
              ),
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(
                          "https://ae01.alicdn.com/kf/HTB1p4MIKVXXXXa.XVXXq6xXFXXXR/1PC-Zakka-16-Lattice-Desktop-Drawer-Storage-Box-Wooden-Retro-Creative-Storage-Cabinet-Living-Room-Decoration.jpg"),
                      fit: BoxFit.cover)),
              otherAccountsPictures: <Widget>[
                GestureDetector(
                    onTap: () => gantiUser(),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(gambar2),
                    ))
              ],
            ),
            const ListTile(
              title: Text("Setting"),
              trailing: Icon(Icons.settings),
            ),
            const ListTile(
              title: Text("Close"),
              trailing: Icon(Icons.close),
            )
          ],
        ),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        // children: daftarSuperhero,
      ),
    );
  }
}
