class Titan {
  double _powerPoint = 0;

  double get powerPoint => _powerPoint;
  set powerPoint(double powerPoint) {
    _powerPoint = powerPoint;

    if (_powerPoint < 5) {
      _powerPoint = 5;
    }
  }
}

