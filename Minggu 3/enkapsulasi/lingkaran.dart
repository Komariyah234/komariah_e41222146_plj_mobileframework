class Lingkaran {
  double _phi = 3.14;
  double _radius = 0;

  // getter
  double get radius => _radius;

  // setter
  set radius(double radius) {
    if (radius < 0) {
      _radius = radius * -1;
      return;
    }

    _radius = radius;
  }

  double get luas => _phi * _radius * _radius;
}
