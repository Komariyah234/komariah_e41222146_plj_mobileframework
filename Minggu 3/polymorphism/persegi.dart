import 'bangun_datar.dart';

class Persegi extends BangunDatar {
  double panjangSisi = 0;

  @override
  double keliling() => 4 * panjangSisi;

  @override
  double luas() => panjangSisi * panjangSisi;
}

