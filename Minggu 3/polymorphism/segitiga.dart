import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  double alas = 0;
  double sisiMiring = 0;
  double tinggi = 0;

  @override
  double luas() => 0.5 * alas * tinggi;

  @override
  double keliling() => alas + sisiMiring + tinggi;
}

