//? function sederhana tanpa return
// void main (){
//   tampilkan();
// }
// tampilkan(){
//   print("Hello, peserta Bootcamp");
// }


//? Function sederhana dengan return
// void main(){
//  print(munculkan_angka());
// }
// munculkan_angka(){
//  return 2;
// }


//?  Function dengan parameter
// void main(){
//  print(kalikan_Dua(6));
// }
// kalikan_Dua(angka){
//  return angka * 2;
// }


//?  Pengiriman parameter lebih dari satu
// void main(){
//  print(kalikan(5,6));
// }
// kalikan(x, y){
//  return x * y;
// }


//?  Inisialisasi parameter dengan nilai default
// void main(){
//  tampilkanangka(6);
// }
// tampilkanangka(n1,{s1:46}){
// print(n1); //hasil akan 5 karena initialisasi 5 didalam value tampilkan
// print(s1); //hasil adalah 45 karena dari parameter diisi 45
// }


//? ANONYMOUS FUNCTION
void main(){
 print(functionPerkalian(5,5));
}
functionPerkalian(angka1, angka2){
 return angka1 * angka2; 
}
