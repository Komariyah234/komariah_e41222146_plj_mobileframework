class Employee {
  final int id;
  final String name;
  final String department;

  Employee(this.id, this.name, this.department);
}

